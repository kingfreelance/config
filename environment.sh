
mkdir webroot
mkdir webroot/assets
mkdir webroot/assets/css
mkdir webroot/assets/js
mkdir webroot/assets/fonts
mkdir webroot/assets/adminlte
mkdir webroot/build
cat > webroot/assets/css/style.min.css
cat > webroot/assets/js/custom.min.js

cp node_modules/config/control-docker.sh .
cp node_modules/config/.htaccess webroot
cp node_modules/config/index.php webroot
cp node_modules/jquery/dist/jquery.min.js webroot/assets/js
cp node_modules/bootstrap/dist/js/bootstrap.min.js webroot/assets/js
cp node_modules/bootbox/dist/bootbox.min.js webroot/assets/js
cp node_modules/popper.js/dist/popper.min.js webroot/assets/js

cp node_modules/bootstrap/dist/css/bootstrap.min.css webroot/assets/css

cp -r node_modules/build/sass webroot/build
cp node_modules/build/gulpfile.js webroot/build

cp -r node_modules/bootstrap/dist/fonts webroot/assets

cp -r node_modules/adminlte/dist/js webroot/assets/adminlte
cp -r node_modules/adminlte/dist/css webroot/assets/adminlte

cp -r node_modules/codeigniter_3/application webroot
cp -r node_modules/codeigniter_3/system webroot

cp -r node_modules/codeigniter_3_template/application webroot

cp node_modules/config/.gitignore webroot

git init
git add .
git commit -m "init"